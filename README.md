# flutter-learning

Flutter learning - FIB

**Folder Final Project**: ./project

**Folder Learning**: ./learning

**Project ideia**

Catalog of animals registered by the user containing the photo (in validation).

**Previous Pictures (W.I.P.):**

1. Login

![image](/uploads/97fd039d4b5fdd726d3a9e7f71b9f1d1/image.png)

2. Menu (light theme)

![image](/uploads/18bbdaf5d2204c5d1535e5b764b5c3b9/image.png)

3. Menu (dark theme)

![image](/uploads/2ea2507d536bd57989d152dd3856d709/image.png)

4. List

![image](/uploads/184167efa1f1ab46b1e96e559140f9bb/image.png)


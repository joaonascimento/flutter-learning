import 'package:flutter/material.dart';
import 'package:learning/controllers/app_controller.dart';
import 'package:learning/pages/login_page.dart';

import '../pages/home_page.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: AppController.instance,
      builder: (context, child) {
        return MaterialApp(
          theme: ThemeData(
            brightness: AppController.instance.isDarkTheme
              ? Brightness.dark
              : Brightness.light,
          ),
          initialRoute: '/',
          routes: {
            '/': (context) => LoginPage(),
            '/home': (context) => HomePage(),
          }
        );
    });
  }
}

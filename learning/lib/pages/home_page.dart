import 'package:flutter/material.dart';
import 'package:learning/controllers/app_controller.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  String theme = AppController.instance.isDarkTheme ? 'Escuro' : 'Claro';

  @override
  Widget build(Object context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            UserAccountsDrawerHeader(
              accountEmail: Text('meuemail@email.com'),
              accountName: Text('João Paulo Nascimento'),
              currentAccountPicture: Image.asset('assets/images/logo.png'),
            ),
            ListTile(
              leading: Icon(Icons.color_lens),
              title: Text('Alternar tema'),
              subtitle: Text('Atual: $theme'),
              onTap: () {
                AppController.instance.changeTheme();
                setState(() {
                  theme = AppController.instance.isDarkTheme ? 'Escuro' : 'Claro';
                });
              },
            ),
            Divider(color: Colors.blue),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Catálogo'),
              subtitle: Text('Página Inicial'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text('Sair'),
              subtitle: Text('Logout da conta'),
              onTap: () {
                Navigator.of(context).pushReplacementNamed('/');
              },
            )
          ],
        )
      ),
      appBar: AppBar(
        title: Text('Catálogo'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child:
            Column(mainAxisAlignment: MainAxisAlignment.center, children: []),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
      ),
    );
  }
}

class SwitchTheme extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Switch(
      value: AppController.instance.isDarkTheme,
      onChanged: (value) {
        AppController.instance.changeTheme();
      },
    );
  }
}

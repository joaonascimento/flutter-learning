import 'package:flutter/material.dart';
import 'package:learning/controllers/app_controller.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email = '';
  String password = '';
  String error = '';

  Widget _body() {
    return SingleChildScrollView(
      child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                  width: 200,
                  height: 200,
                  child: Image.asset('assets/images/logo.png')),
              Container(height: 10),
              Card(
                color: AppController.instance.isDarkTheme
                    ? Colors.black26
                    : Colors.white70,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: [
                      TextField(
                        onChanged: (value) {
                          email = value;
                        },
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          labelText: 'Email',
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 10),
                      TextField(
                        onChanged: (value) {
                          password = value;
                        },
                        obscureText: true,
                        decoration: InputDecoration(
                            labelText: 'Senha', border: OutlineInputBorder()),
                      ),
                      SizedBox(height: 15),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RaisedButton(
                              color: Colors.red,
                              textColor: Colors.white,
                              onPressed: () {
                                Navigator.of(context)
                                    .pushReplacementNamed('/home');
                              },
                              child: Text('Registrar',
                                  textAlign: TextAlign.center),
                            ),
                            SizedBox(width: 20),
                            RaisedButton(
                              color: Colors.blueAccent,
                              textColor: Colors.white,
                              onPressed: () {
                                final snackBar = SnackBar(
                                  content: Text('Usuário não encontrado!'),
                                  backgroundColor: Colors.red,
                                );

                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);

                                if (email == 'joao@teste.com' &&
                                    password == '123') {
                                  Navigator.of(context)
                                      .pushReplacementNamed('/home');
                                }
                              },
                              child:
                                  Text('Entrar', textAlign: TextAlign.center),
                            ),
                          ]),
                    ],
                  ),
                ),
              ),
            ]),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(color: Colors.green),
          _body(),
        ],
      ),
    );
  }
}
